import Vue from 'vue';
import App from './App.vue';
import ConversationList from './conversation-list.vue';
import ConversationSingle from './conversation-single.vue';
import SentFolder from './sent-folder.vue';

var VueRouter = require('vue-router');
Vue.use(VueRouter);
var router = new VueRouter();

router.map({
    '/': {
        component: ConversationList
    },
    '/starred': {
        component: ConversationList
    },
    '/sent': {
        component: SentFolder
    },
    '/tags/:tag': {
        component: ConversationList
    },
    '/conversations/:conversation': {
        component: ConversationSingle
    }
})
router.start(App, '#app');
