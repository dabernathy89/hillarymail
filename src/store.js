var Vue = require('vue');
var VueResource = require('vue-resource');
import eventbus from './event-bus.js';

var store = {};

// Use these to stop checking the server if they don't return any data the first time.
// Avoids infinite loopage
var checkForConversations = true;
var checkForUsers = true;
var checkForTags = true;
var checkForOptions = true;

var cache = {
    conversations: [],
    users: [],
    tags: {},
    options: {}
};

Vue.use(VueResource);

Vue.http.options.root = '/wp-json';
if (__DEV__) {
    Vue.http.options.root = 'http://hillarysinbox.dev/wp-json';
}

module.exports = store;

store.fetchData = function() {
    store.getConversations();
    store.getUsers();
    store.getTags();
    store.getOptions();
};

store.maybeEmitAllDataLoaded = function() {
    if ( ! checkForConversations && ! checkForUsers && ! checkForTags && ! checkForOptions ) {
        eventbus.emit('all-data-loaded');
    }
}

eventbus.on('fetch-data', store.fetchData);

function objectIsEmpty( obj ) {
    for ( var x in obj ) {
        return false;
    }

    return true;
}

store.getConversations = function() {
    if ( ! cache.conversations.length && checkForConversations ) {
        Vue.http.get('wp/v2/posts', {
            params: {
                '_embed': '1',
                'per_page': '100'
            }
        }).then(function(response) {

            cache.conversations = store.transformConversations( response.body );

            checkForConversations = false;

            eventbus.emit('conversations-loaded');

            store.maybeEmitAllDataLoaded();

        }, function(response) {
            checkForConversations = false;
        });
    }

    return cache.conversations;
};

store.getUsers = function() {
    if ( ! cache.users.length && checkForUsers ) {
        Vue.http.get('wp/v2/mailusers', {
            params: {
                '_embed': '1',
                'per_page': '100'
            }
        }).then(function(response) {

            cache.users = store.transformUsers( response.body );

            checkForUsers = false;

            eventbus.emit('users-loaded');

            store.maybeEmitAllDataLoaded();

        }, function(response) {
            checkForUsers = false;
        });
    }

    return cache.users;
};

store.getTags = function() {
    if ( objectIsEmpty(cache.tags) && checkForTags ) {
        Vue.http.get('wp/v2/tags', {
            params: {
                '_embed': '1',
                'per_page': '100'
            }
        }).then(function(response) {

            cache.tags = store.transformTags( response.body );

            checkForTags = false;

            eventbus.emit('tags-loaded');

            store.maybeEmitAllDataLoaded();

        }, function(response) {
            checkForTags = false;
        });
    }

    return cache.tags;
};

store.getOptions = function() {
    if ( objectIsEmpty(cache.options) && checkForOptions ) {
        Vue.http.get('hillarymail/v1/options', {
        }).then(function(response) {

            cache.options = response.body;

            checkForOptions = false;

            eventbus.emit('options-loaded');

            store.maybeEmitAllDataLoaded();

        }, function(response) {
            checkForOptions = false;
        });
    }

    return cache.options;
};

store.transformConversations = function( raw_conversations ) {
    var conversations = [];

    raw_conversations.forEach(function(conv){
        conversations.push({
            id: conv.id,
            title: conv.title.rendered,
            tags: conv.tags_full,
            emails: conv.emails,
            is_read: conv.is_read,
            is_starred: conv.is_starred,
            tag_string: conv.tag_string,
            all_emails_string: '',
            deleted: false
        });
    });

    return conversations;
};

store.transformUsers = function( raw_users ) {
    var users = [];

    raw_users.forEach(function(user){
        users.push({
            id: user.id,
            name: user.title.rendered,
            short_name: user.short_name,
            image: user.user_photo,
            is_active: user.chat_status,
            messages: user.chat_messages,
            messageStack: [],
            chatIsHidden: true,
            usedMessageCount: 0
        });
    });

    return users;
};

store.transformTags = function( raw_tags ) {
    var tags = {};

    raw_tags.forEach(function(tag){
        tags[tag.slug] = {
            background: tag.hmail_colors.background,
            text: tag.hmail_colors.text,
            slug: tag.slug,
            name: tag.name
        };
    });

    return tags;
};
