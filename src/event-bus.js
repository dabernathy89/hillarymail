var EventEmitter = require('events');

var eventbus = new EventEmitter;

eventbus.setMaxListeners(20);

module.exports = eventbus;