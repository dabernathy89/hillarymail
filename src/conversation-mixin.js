var moment = require('moment');

module.exports = {
    methods: {
        userNameById: function(id) {
            for (var i = 0; i < this.users.length; i++ ) {
                if (this.users[i].id === id) {
                    return this.users[i].name;
                }
            }

            return null;
        },

        getFormattedTime: function(time, format) {
            if (typeof format === "undefined") {
                format = 'h:mm a';
            }
            var dateobj = moment(time);
            return dateobj.format(format);
        },

        timeFromNow: function(time) {
            return moment(time).fromNow();
        }
    },

    computed: {
        authorsList: function() {
            var authors = [];
            var username;

            for ( var i = 0; i < this.conversation.emails.length; i++ ) {
                username = this.userNameById(this.conversation.emails[i].from[0]);
                if ( username === "Hillary Clinton" ) {
                    if ( authors.indexOf('Me') < 0 ) {
                        authors.push('Me');
                    }
                } else if ( authors.indexOf(username) < 0 ) {
                    authors.push(username);
                }
            }

            return authors.join(', ');
        }
    }
};
